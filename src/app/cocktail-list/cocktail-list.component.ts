import { Component, OnInit } from '@angular/core';
import { CocktailService } from '../cocktail.service';
import { Cocktail } from '../cocktail.model';

@Component({
  selector: 'app-cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.scss']
})
export class CocktailListComponent implements OnInit {

  public cocktails:Cocktail[] = [] ;


  constructor(private cocktailService: CocktailService) {
  }

  ngOnInit(): void {
    this.cocktailService.getCocktails().subscribe(cocktails=>{
      this.cocktails = cocktails;
      console.log(this.cocktails);

    });
  }

}
